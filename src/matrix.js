import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
const generateDefualtMatrix = (matrix, callback) => {
  const newColumnArr = [];
  for (let i = 0; i < matrix.length; i++) {
    const row = matrix[i];
    const newRow = [];
    for (let j = 0; j < row.length; j++) {
      newRow.push(
        <TouchableOpacity
          style={styles.cell}
          onPress={() => {
            callback({i, j});
          }}
          style={styles.cell}
          key={`cell_${i}_${j}`}>
          <Text style={styles.cellText}>{matrix[i][j]}</Text>
        </TouchableOpacity>,
      );
    }
    newColumnArr.push(
      <View style={styles.row} key={`row_${i}`}>
        {newRow}
      </View>,
    );
  }
  return newColumnArr;
};
export const Matrix = props => {
  const {matrix,handleClick} = props;
  return <>{generateDefualtMatrix(matrix,handleClick)}</>;
};

const styles = {
  row: {
    flexDirection: 'row',
  },
  cell: {
    width: 100,
    height: 100,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  cellText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'red',
  },
};
