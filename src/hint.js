import React from 'react';
import {Text} from 'react-native';
export const Hint = (props)=>{
    const {hint} = props
    return(
        <> 
          <Text style={styles.hint}>{props.children}</Text>
        </>
    )
} 
const styles = {
  hint:{
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 30,
  }
}
