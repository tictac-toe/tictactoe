/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from "react";
import type { Node } from "react";
import { SafeAreaView, TouchableOpacity, View, Text } from "react-native";
import { Matrix, Hint } from "./src/index";
const ticTacToeMatrix = Array(3)
  .fill()
  .map(() => Array(3).fill(null));
const xSymbol = "\u2715";
const oSymbol = "\u004F";

const getWinner = (matrix) => {
  let transposed = [[], [], []];
  let diagonals = [[], []];
  for (let i = 0; i < matrix.length; i++) {
    const row = matrix[i];
    for (let j = 0; j < row.length; j++) {
      transposed[i][j] = matrix[j][i];
      if (i === j) {
        diagonals[0][i] = matrix[i][j];
      }
      if (i === Math.abs(j - (row.length - 1))) {
        diagonals[1][i] = matrix[i][j];
      }
    }
  }
  const allLines = matrix.concat(transposed).concat(diagonals);
  for (let i = 0; i < allLines.length; i++) {
    const line = allLines[i];
    const isEqual = line.every((item) => item === line[0]);
    if (isEqual) {
      return line[0];
    }
  }

  return null;
};
const App: () => Node = () => {
  const [gameMatrix, setgameMatrix] = useState(ticTacToeMatrix);
  const [turnUser, setTurnUser] = useState(xSymbol);
  const [winnerUser, setWinnerUser] = useState();

  const handleClickUser = (obj) => {
    const { i, j } = obj;
    if (winnerUser || gameMatrix[i][j]) {
      return;
    }
    gameMatrix[i][j] = turnUser;
    setTurnUser(turnUser === xSymbol ? oSymbol : xSymbol);
    setgameMatrix(gameMatrix);
    const winner = getWinner(gameMatrix);
    if (winner) {
      alert(`${winner} wins!`);
      setWinnerUser(`${winner} wins!`);
    }
  };
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.subMain1}>
        {!winnerUser && <Hint>{`The Turn Now  for user ${turnUser}`}</Hint>}
        <Hint>{winnerUser}</Hint>
      </View>
      <View style={styles.main}>
        <Matrix matrix={gameMatrix} handleClick={handleClickUser}></Matrix>
      </View>
      <View style={styles.subMain1}>
        <TouchableOpacity
          onPress={() => {
            setgameMatrix(
              Array(3)
                .fill()
                .map(() => Array(3).fill(null))
            );
            setTurnUser(xSymbol);
            setWinnerUser(undefined);
          }}
        >
          <Text style={{ fontSize: 24 }}>Restart</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};
const styles = {
  subMain1: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  main: {
    flex: 6,
    justifyContent: "center",
    alignItems: "center",
  },
};

export default App;
